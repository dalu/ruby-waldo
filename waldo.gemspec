Gem::Specification.new do |s|
    s.name        = 'waldo'
    s.version     = '0.1.2'
    s.summary     = "Localisation library"
    s.description =  <<~EOF
      
      Localisation using TDoA (NLM, CHAN)

      EOF

    s.homepage    = 'https://gitlab.inria.fr/dalu/waldo'
    s.license     = 'Apache-2.0'

    s.authors     = [ "Stéphane D'Alu" ]
    s.email       = [ 'stephane.dalu@insa-lyon.fr' ]

    s.extensions = [ 'ext/extconf.rb' ]
    s.files      = %w[ waldo.gemspec ]  +
                   Dir['ext/**/*.{c,h,rb,inc}']
end
