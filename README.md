# ruby-waldo

Binding to the waldo library

## Getting started

~~~
require 'waldo'		# Waldo library
require 'matrix'	# Matrix/Vector (separate gem since ruby 3.1)

$e        = Waldo.random_xyz_ball(2.0)  # Emitter position (randomly choosen)
$velocity = 299702547.0                 # Propagation speed
$base     = [ [ -2,  0,  3 ],           # Receiver array
              [  8, -2, -5 ],
              [ 12, -5,  7 ],
              [ 20,  1,-13 ],
              [ -3,-12,  9 ] ]

# Simulate signal reception
$tdoa_base = $base.map {|b| b + [ (Vector[*b]-Vector[*$e]).norm / $velocity ] }

# Estimate position from received signal
Waldo.xyz_tdoa_nlm($tdoa_base, $velocity)
~~~
