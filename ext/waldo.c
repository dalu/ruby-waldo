#include <ruby.h>
#include <waldo.h>
#include <waldo/coordinates.h>
#include <waldo/random.h>
#include <waldo/tdoa.h>
#include <waldo/toa.h>
#include <waldo/optimizer.h>

#define IF_UNDEF(a, b)				\
    ((a) == Qundef) ? (b) : (a)

#if !defined(XYZ_START_FACTOR)
#define XYZ_START_FACTOR 10.0
#endif

#define SEARCH_START_PROJECTED_OCTANT		1
#define SEARCH_START_NORMALIZED_BARYCENTER	2

#ifndef SEARCH_START
#define SEARCH_START SEARCH_START_NORMALIZED_BARYCENTER
#endif

#if SEARCH_START == SEARCH_START_NORMALIZED_BARYCENTER
#define SEARCH_START_STR "normalized-barycenter"
#elif SEARCH_START == SEARCH_START_PROJECTED_OCTANT
#define SEARCH_START_STR "projected-octant"
#else
#error Unknown search start
#endif

#define XYZ_IS_NAN(xyz)						\
    (isnan((xyz)->x) || isnan((xyz)->y) || isnan((xyz)->y))


static void
_xyzt_base_estimated_search_start(waldo_xyz_t *start,
				  int bsize, waldo_xyzt_t *base,
				  waldo_real_t factor)
{
    waldo_xyz_t bary = { 0 };
    for (int i = 0 ; i < bsize ; i++) {
	waldo_xyz_t weighted;
	waldo_xyz_scale(&weighted, &base[i].xyz, 1.0 / base[i].t);
	waldo_xyz_add(&bary, &bary, &weighted);
    }
#if SEARCH_START == SEARCH_START_NORMALIZED_BARYCENTER
    waldo_real_t norm = waldo_xyz_norm(&bary);
    if (norm >= 1.0) 
	factor /= norm;
    waldo_xyz_scale(start, &bary, factor);
#elif SEARCH_START == SEARCH_START_PROJECTED_OCTANT
    start->x = bary.x > 0.0 ? factor : -factor;
    start->y = bary.y > 0.0 ? factor : -factor;
    start->z = bary.z > 0.0 ? factor : -factor;
#endif
}


static void
_xyzt_base(VALUE base, int c_size, waldo_xyzt_t *c_base)
{
    Check_Type(base, T_ARRAY);
    if (c_size != RARRAY_LEN(base))
	rb_fatal("incorrect base size");

    for (int i = 0 ; i < c_size ; i++) {
	VALUE xyzt = RARRAY_AREF(base, i);
	if (!RB_TYPE_P(xyzt, T_ARRAY) || (RARRAY_LEN(xyzt) != 4))
	    rb_raise(rb_eArgError,
		     "base element %d is not an xyzt coordinate", i);
	for (int j = 0 ; j < 4 ; j++) 
	    c_base[i].axis[j] = NUM2DBL(RARRAY_AREF(xyzt, j));
    }
}


/*======================================================================*/

static ID id_epsilon;
static ID id_iterations;
static ID id_start;

static ID id_EPSILON;
static ID id_ITERATIONS;

static VALUE mWaldo = Qundef;
static VALUE v1_0   = Qundef;


static VALUE set_seed(VALUE self, VALUE val) {
    long long number = NUM2LL(val);
    unsigned short seed[3] = { (number >>  0) & 0xFFFF,
			       (number >> 16) & 0xFFFF,
			       (number >> 32) & 0xFFFF };
    seed48(seed);
    return self;
}

static VALUE
random_xyz_sphere(int argc, VALUE* argv, VALUE self)
{
    VALUE radius;
    rb_scan_args(argc, argv, "01", &radius);
    if (NIL_P(radius)) radius = v1_0;

    waldo_xyz_t a;
    waldo_random_xyz_sphere(&a, NUM2DBL(radius));
    return rb_ary_new_from_args(3, DBL2NUM(a.x), DBL2NUM(a.y), DBL2NUM(a.z));
}

static VALUE
random_xyz_thick_sphere(int argc, VALUE* argv, VALUE self)
{
    VALUE radius;
    rb_scan_args(argc, argv, "01", &radius);
    if (NIL_P(radius)) radius = v1_0;

    waldo_xyz_t a;
    waldo_random_xyz_sphere(&a, NUM2DBL(radius));
    return rb_ary_new_from_args(3, DBL2NUM(a.x), DBL2NUM(a.y), DBL2NUM(a.z));
}

static VALUE
random_xyz_ball(int argc, VALUE* argv, VALUE self)
{
    VALUE r1, r2;
    rb_scan_args(argc, argv, "02", &r1, &r2);
    if (NIL_P(r1)) r1 = v1_0;
    
    waldo_xyz_t a;
    if (NIL_P(r2)) { waldo_random_xyz_ball(&a, NUM2DBL(r1));                   }
    else           { waldo_random_xyz_hollow_ball(&a,NUM2DBL(r1),NUM2DBL(r2)); }
    return rb_ary_new_from_args(3, DBL2NUM(a.x), DBL2NUM(a.y), DBL2NUM(a.z));
}

    

static VALUE
xyz_toa_nlm(int argc, VALUE* argv, VALUE self)
{
    // Retrieve arguments
    VALUE xyzt_base, propagation_speed, opts;
    VALUE kwargs[3];	
    rb_scan_args(argc, argv, "2:", &xyzt_base, &propagation_speed, &opts);
    rb_get_kwargs(opts, (ID[]){ id_epsilon, id_iterations, id_start
	                      }, 0, 3, kwargs);
    VALUE epsilon    = IF_UNDEF(kwargs[0], rb_const_get(mWaldo, id_EPSILON   ));
    VALUE iterations = IF_UNDEF(kwargs[1], rb_const_get(mWaldo, id_ITERATIONS));
    VALUE start      = IF_UNDEF(kwargs[2], Qnil);
    
    // Type check
    Check_Type(xyzt_base,         T_ARRAY);
    Check_Type(propagation_speed, T_FLOAT);
    Check_Type(epsilon,           T_FLOAT);
    Check_Type(iterations,        T_FIXNUM);

    if (RB_TYPE_P(start, T_ARRAY) && (RARRAY_LEN(start) != 3)) {
	rb_raise(rb_eArgError, "start is not an xyz coordinate");
    }
    
    // Populating xyzt base
    int bsize = RARRAY_LENINT(xyzt_base);
    waldo_xyzt_t base[bsize];
    _xyzt_base(xyzt_base, bsize, base);

    // Starting point
    waldo_xyz_t c_start;
    if (RB_TYPE_P(start, T_ARRAY)) {
	for (int i = 0 ; i < 3 ; i++)
	    c_start.axis[i] = NUM2DBL(RARRAY_AREF(start, i));
    } else {
	waldo_real_t factor = NIL_P(start) ? XYZ_START_FACTOR : NUM2DBL(start);
	if (factor <= 0.0) {
	    rb_raise(rb_eArgError, "start factor need to be > 0.0");
	}
	_xyzt_base_estimated_search_start(&c_start, bsize, base, factor);
    }
    
    // ToA
    waldo_xyz_t estimated;
    int rc = waldo_xyz_toa_nlm(bsize, base, RFLOAT_VALUE(propagation_speed),
			       &c_start, &estimated,
			       NUM2INT(iterations), RFLOAT_VALUE(epsilon));
    if ((rc != WALDO_OK) || XYZ_IS_NAN(&estimated))
	return Qnil;
    
    return rb_ary_new_from_args(3, DBL2NUM(estimated.x),
				   DBL2NUM(estimated.y),
				   DBL2NUM(estimated.z));
}


static VALUE
xyz_tdoa_nlm(int argc, VALUE* argv, VALUE self)
{
    // Retrieve arguments
    VALUE xyzt_base, propagation_speed, opts;
    VALUE kwargs[3];	
    rb_scan_args(argc, argv, "2:", &xyzt_base, &propagation_speed, &opts);
    rb_get_kwargs(opts, (ID[]){ id_epsilon, id_iterations, id_start
	                      }, 0, 3, kwargs);
    VALUE epsilon    = IF_UNDEF(kwargs[0], rb_const_get(mWaldo, id_EPSILON   ));
    VALUE iterations = IF_UNDEF(kwargs[1], rb_const_get(mWaldo, id_ITERATIONS));
    VALUE start      = IF_UNDEF(kwargs[2], Qnil);
    
    // Type check
    Check_Type(xyzt_base,         T_ARRAY);
    Check_Type(propagation_speed, T_FLOAT);
    Check_Type(epsilon,           T_FLOAT);
    Check_Type(iterations,        T_FIXNUM);

    if (RB_TYPE_P(start, T_ARRAY) && (RARRAY_LEN(start) != 3)) {
	rb_raise(rb_eArgError, "start is not an xyz coordinate");
    }
    
    // Populating xyzt base
    int bsize = RARRAY_LENINT(xyzt_base);
    waldo_xyzt_t base[bsize];
    _xyzt_base(xyzt_base, bsize, base);

    // Starting point
    waldo_xyz_t c_start;
    if (RB_TYPE_P(start, T_ARRAY)) {
	for (int i = 0 ; i < 3 ; i++)
	    c_start.axis[i] = NUM2DBL(RARRAY_AREF(start, i));
    } else {
	waldo_real_t factor = NIL_P(start) ? XYZ_START_FACTOR : NUM2DBL(start);
	if (factor <= 0.0) {
	    rb_raise(rb_eArgError, "start factor need to be > 0.0");
	}
	_xyzt_base_estimated_search_start(&c_start, bsize, base, factor);
    }
    
    // TDoA
    waldo_xyz_t estimated;
    int rc = waldo_xyz_tdoa_nlm(bsize, base, RFLOAT_VALUE(propagation_speed),
			       &c_start, &estimated,
			       NUM2INT(iterations), RFLOAT_VALUE(epsilon));
    if ((rc != WALDO_OK) || XYZ_IS_NAN(&estimated))
	return Qnil;
    
    return rb_ary_new_from_args(3, DBL2NUM(estimated.x),
				   DBL2NUM(estimated.y),
				   DBL2NUM(estimated.z));
}


static VALUE
xyZ_tdoa_nlm(int argc, VALUE* argv, VALUE self)
{
    // Retrieve arguments
    VALUE xyzt_base, propagation_speed, z, opts;
    VALUE kwargs[3];	
    rb_scan_args(argc, argv, "3:", &xyzt_base, &propagation_speed, &z, &opts);
    rb_get_kwargs(opts, (ID[]){ id_epsilon, id_iterations, id_start
	                      }, 0, 3, kwargs);
    VALUE epsilon    = IF_UNDEF(kwargs[0], rb_const_get(mWaldo, id_EPSILON   ));
    VALUE iterations = IF_UNDEF(kwargs[1], rb_const_get(mWaldo, id_ITERATIONS));
    VALUE start      = IF_UNDEF(kwargs[2], Qnil);
    
    // Type check
    Check_Type(xyzt_base,         T_ARRAY);
    Check_Type(propagation_speed, T_FLOAT);
    Check_Type(epsilon,           T_FLOAT);
    Check_Type(iterations,        T_FIXNUM);

    if (RB_TYPE_P(start, T_ARRAY) && (RARRAY_LEN(start) != 3)) {
	rb_raise(rb_eArgError, "start is not an xyz coordinate");
    }
    
    // Populating xyzt base
    int bsize = RARRAY_LENINT(xyzt_base);
    waldo_xyzt_t base[bsize];
    _xyzt_base(xyzt_base, bsize, base);

    // Starting point
    waldo_xyz_t c_start;
    if (RB_TYPE_P(start, T_ARRAY)) {
	for (int i = 0 ; i < 3 ; i++)
	    c_start.axis[i] = NUM2DBL(RARRAY_AREF(start, i));
    } else {
	waldo_real_t factor = NIL_P(start) ? XYZ_START_FACTOR : NUM2DBL(start);
	if (factor <= 0.0) {
	    rb_raise(rb_eArgError, "start factor need to be > 0.0");
	}
	_xyzt_base_estimated_search_start(&c_start, bsize, base, factor);
    }
    c_start.z = NUM2DBL(z);
    
    // TDoA
    waldo_xyz_t estimated;
    int rc = waldo_xyZ_tdoa_nlm(bsize, base, RFLOAT_VALUE(propagation_speed),
			       &c_start, &estimated,
			       NUM2INT(iterations), RFLOAT_VALUE(epsilon));
    if ((rc != WALDO_OK) || XYZ_IS_NAN(&estimated))
	return Qnil;
    
    return rb_ary_new_from_args(3, DBL2NUM(estimated.x),
				   DBL2NUM(estimated.y),
				   DBL2NUM(estimated.z));
}




    


static VALUE
xyz_tdoa_chan_ab(VALUE self, VALUE tdoa_base, VALUE propagation_speed)
{
    // Type check
    Check_Type(tdoa_base,         T_ARRAY);
    Check_Type(propagation_speed, T_FLOAT);

    // Populating xyzt tdoa base
    int bsize = RARRAY_LENINT(tdoa_base);
    waldo_xyzt_t base[bsize];
    _xyzt_base(tdoa_base, bsize, base);

    // Propagation speed
    double speed = RFLOAT_VALUE(propagation_speed);

    // TDoA Chan
    waldo_xyz_t a_far, a_near, b_near, b_far;
    int n = waldo_xyz_tdoa_chan_ab(bsize, base, speed,
				   &a_far, &a_near, &b_near, &b_far);

    VALUE val[4] = { Qnil, Qnil, Qnil, Qnil };
    switch(n) {
    case 0:
	return Qnil;
    case 4:
	if (! XYZ_IS_NAN(&b_far))
	    val[3] = rb_ary_new_from_args(3, DBL2NUM(b_far .x),
					     DBL2NUM(b_far .y),
					     DBL2NUM(b_far .z));
    case 3:
	if (! XYZ_IS_NAN(&b_near))
	    val[2] = rb_ary_new_from_args(3, DBL2NUM(b_near.x),
					     DBL2NUM(b_near.y),
				             DBL2NUM(b_near.z));
    case 2:
	if (! XYZ_IS_NAN(&a_near))
	    val[1] = rb_ary_new_from_args(3, DBL2NUM(a_near.x),
					     DBL2NUM(a_near.y),
				             DBL2NUM(a_near.z));
    case 1:
	if (! XYZ_IS_NAN(&a_far))
	    val[0] = rb_ary_new_from_args(3, DBL2NUM(a_far .x),
					     DBL2NUM(a_far .y),
				             DBL2NUM(a_far .z));
	break;
    default:
	rb_fatal("unsupported return value");
    }
    
    return rb_ary_new_from_values(n, val);
}
    


void Init_waldo()
{
    id_epsilon        = rb_intern_const("epsilon"   );
    id_iterations     = rb_intern_const("iterations");
    id_start          = rb_intern_const("start");

    id_EPSILON        = rb_intern_const("EPSILON"   );
    id_ITERATIONS     = rb_intern_const("ITERATIONS");

    mWaldo = rb_define_module("Waldo");
    v1_0   = DBL2NUM(1.0);
    
    rb_define_const(mWaldo, "LIB_VERSION",  rb_str_new_cstr(waldo_version()));
    rb_define_const(mWaldo, "EPSILON",      DBL2NUM(WALDO_EPSILON));
    rb_define_const(mWaldo, "ITERATIONS",   INT2NUM(WALDO_ITERATIONS));
    rb_define_const(mWaldo, "SEARCH_START", rb_str_new_cstr(SEARCH_START_STR));
    
    rb_define_module_function(mWaldo, "xyz_toa_nlm",      xyz_toa_nlm,     -1);
    rb_define_module_function(mWaldo, "xyz_tdoa_chan_ab", xyz_tdoa_chan_ab, 2);
    rb_define_module_function(mWaldo, "xyz_tdoa_nlm",     xyz_tdoa_nlm,    -1);
    rb_define_module_function(mWaldo, "xyZ_tdoa_nlm",     xyZ_tdoa_nlm,    -1);
    rb_define_module_function(mWaldo, "random_xyz_sphere",
			      random_xyz_sphere, -1);
    rb_define_module_function(mWaldo, "random_xyz_ball",
			      random_xyz_ball, -1);
    
    rb_define_singleton_method(mWaldo, "seed=", set_seed, 1);
}


