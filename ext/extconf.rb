require 'mkmf'

#$LDFLAGS  += " -L ../build -lm"
#$INCFLAGS += " -I ../include"
#have_library('waldo')

if RbConfig::CONFIG['target_os'] = /^freebsd/
  $PKGCONFIG = '/usr/local/bin/pkg-config'
end

cflags, ldflags, libs = pkg_config('waldo')

$LDFLAGS  += " #{ldflags} #{libs}"
$INCFLAGS += " #{cflags}"
have_library('waldo')

create_makefile("waldo")
