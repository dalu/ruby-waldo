require 'waldo'

begin
    require 'matrix'
rescue LoadError
    $stderr.puts "On ruby >= 3.1, matrix is now shipped as a separate gem"
end

$base = [ [-2,  0,  3],
          [ 8, -2, -5],
          [12, -5,  7],
          [20,  1,-13],
          [-3,-12,  9] ]
$e    = [ 3.00, 2.20, 2.60]

# $base = [ [  0.0,  0.0,                 1.0 ],
#           [  0.0,  0.0,                -1.0 ],
#           [  1.0,  0.0,                 0.0 ],
#           [ -0.5,  Math.sqrt(3.0)/2.0,  0.0 ],
#           [ -0.5, -Math.sqrt(3.0)/2.0,  0.0 ] ]
# $e    = Waldo.random_xyz_ball(100.0)



class World
    PROPAGATION_SPEED = 299702547.0

    def initialize(antenna_error: 0, clock_precision: 64e9)
        @antenna_error   = antenna_error
        @clock_precision = clock_precision
    end

    def uwb_propagation_time(a, b)
        t = (Vector[*a] - Vector[*b]).norm / PROPAGATION_SPEED
        e = if @antenna_error > 0
            then Random.rand(-@antena_error .. @antena_error)
            else 0.0
            end
        (t * @clock_precision + e).floor / @clock_precision
    end

    def self.best(list, real)
        list.min {|estimated| (Vector[*estimated] - Vector[*real]).norm }
    end

end


$w         = World.new
$xyzt_base = $base.map {|p| p + [ $w.uwb_propagation_time(p, $e) ] }
$speed     = World::PROPAGATION_SPEED
$estimated = {
    'ToA'              => Waldo.xyz_toa_nlm($xyzt_base, $speed),
    'TDoA CHAN (best)' => World.best(Waldo.xyz_tdoa_chan_ab($xyzt_base, $speed),$e),
    'TDoA NLM'         => Waldo.xyz_tdoa_nlm($xyzt_base, $speed),
    'TDoA NLM (xyZ)'   => Waldo.xyZ_tdoa_nlm($xyzt_base, $speed, $e[2])
}


puts "libwaldo version : #{Waldo::LIB_VERSION}"
puts
puts "Receiver base:"
$base.each_with_index do |xyz, i|
    puts "  [%i] = %6.2f, %6.2f, %6.2f" % [i, *xyz]
end
puts
puts "Positions   : %f, %f, %f" % $e
puts "Estimated   :"
$estimated.each do |name, value|
    if value.nil?
    then puts " %18s : - no result -" % [ name ]
    else puts " %18s : %8.4f, %8.4f, %8.4f (err = %8.4f)" % [
                  name, *value, (Vector[*value] - Vector[*$e]).norm ]
    end
end

puts
puts "Random points:"
puts " - %6.2f, %6.2f, %6.2f" % Waldo.random_xyz_sphere
puts " - %6.2f, %6.2f, %6.2f" % Waldo.random_xyz_ball(2.0)

